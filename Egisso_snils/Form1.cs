﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Diagnostics;
using System.Xml;

namespace Egisso_snils
{
    public partial class Form1 : Form
    {
        StreamReader sr;
        List<ValidationRequest> ListRequests = new List<ValidationRequest>();

        public Form1()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                while (!sr.EndOfStream)
                {
                    string Line = sr.ReadLine();

                    ValidationRequest parsed = FileParser.ParseFromLine(Line);

                    if (FileParser.ErrorText != "")
                    {
                        textBox2.Text += parsed.GetFIOandId() +" "+ FileParser.ErrorText;
                    }


                    textBox1.Text += parsed.GetTextValidation();
                }
            } catch (Exception ex)
            {
                textBox2.Text = FormLogOnError(this, ex);
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            try
            {
                sr = new StreamReader("Книга1.csv");
                ValidationRequest.stat_id = Convert.ToInt32(textBox3.Text);
            }
            catch (Exception ex)
            {
                textBox2.Text = FormLogOnError(this, ex);
            }
        }

        public string FormLogOnError(object classobject, Exception ex)
        {
            string TextError, ErrorMethod, ErrorClass;
            int LineError;

            StackTrace st = new StackTrace(ex, true);
            StackFrame[] ErrorFrames = st.GetFrames();


            TextError = ex.Message + Environment.NewLine;
            TextError += "----------- Трасировка стека --------------------------" + Environment.NewLine;
            foreach (StackFrame ErrorFrame in ErrorFrames)
            {

                ErrorMethod = ErrorFrame.GetMethod().Name;
                LineError = ErrorFrame.GetFileLineNumber();
                ErrorClass = ErrorFrame.GetFileName();

                TextError += "Модуль: " + ErrorClass + Environment.NewLine;
                TextError += "Метод: " + ErrorMethod + Environment.NewLine;
                TextError += "Строка: " + LineError.ToString() + Environment.NewLine + Environment.NewLine;

            }
            TextError += "-----------Конец Трассировки--------------------------" + Environment.NewLine;

            return TextError;
        }

    }
}
