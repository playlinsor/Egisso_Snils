﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;

namespace Krab112
{
  
    class LogViewMain
    {
        static public SortedList<string, string> MainList = new SortedList<string, string>();
        static string CurrentKey; 



        static public void SaveInLog(string Text,string KeyWrite)
        {
            if (KeyWrite == null || KeyWrite == "") KeyWrite = "Призрак";
            if (MainList.ContainsKey(KeyWrite))
            {
                MainList[KeyWrite] = Text;
            } else
            {
                MainList.Add(KeyWrite,Text);
            }
        }

        static public void SaveInLogError(string Text)
        {
            DateTime dt = DateTime.Now;
            string id = "Error#" + dt.ToString("H:mm:ss");
            if (MainList.ContainsKey(id)) id += dt.ToString(".FFF");
            MainList.Add(id, Text);

        } 

        static public string FormLogOnError(object classobject, Exception ex)
        {
            string TextError, ErrorMethod, ErrorClass;
            int LineError;

            StackTrace st = new StackTrace(ex, true);
            StackFrame[] ErrorFrames = st.GetFrames();


            TextError = ex.Message + Environment.NewLine;
            TextError += "----------- Трасировка стека --------------------------" + Environment.NewLine;
            foreach (StackFrame ErrorFrame in ErrorFrames)
            {

                ErrorMethod = ErrorFrame.GetMethod().Name;
                LineError = ErrorFrame.GetFileLineNumber();
                ErrorClass = ErrorFrame.GetFileName();

                TextError += "Модуль: " + ErrorClass + Environment.NewLine;
                TextError += "Метод: " + ErrorMethod + Environment.NewLine;
                TextError += "Строка: " + LineError.ToString() + Environment.NewLine + Environment.NewLine;

            }
            TextError += "-----------Конец Трассировки--------------------------" + Environment.NewLine;

            return TextError;
        }

        static public string GetStringError()
        {
            return "Внимание! Выполнение программы прекращено из за фатальной ошибки, подробности в отчете";
        }

    }

}
