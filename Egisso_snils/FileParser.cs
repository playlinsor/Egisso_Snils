﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egisso_snils
{
    class FileParser
    {
        public static string ErrorText = "";

        public static ValidationRequest ParseFromLine(string Line)
        {
            ValidationRequest Temp = new ValidationRequest();

            string[] LineGroup = Line.Split(';');
            string[] filedsFIO = ParseFIO(LineGroup[0]);
            string fieldsSnils = ParseSnils(LineGroup[1]);
            string fieldsDate = ParseData(LineGroup[2]);
            string fieldsGender = ParseGender(LineGroup[3]);

            ErrorText = "";
            if (filedsFIO.Length != 3) ErrorText = "Ошибка преобразования ФИО"+Environment.NewLine;
            if (fieldsSnils.Length != 11 && fieldsSnils.Length!=0) ErrorText += "Ошибка преобразования Снилс" + Environment.NewLine;
            if (fieldsDate.Length != 10) ErrorText += "Ошибка преобразования Даты" + Environment.NewLine;
            if (fieldsDate == "-") ErrorText += "Ошибка преобразования Пола" + Environment.NewLine;

            if (LineGroup.Length == 5 && LineGroup[4] != "")
            {
                string[] Address = ParseAddres(LineGroup[4]);
               
                string filedGorod = LineGroup[4];
                Temp.WriteNew(0, filedsFIO[0], filedsFIO[1], filedsFIO[2], fieldsDate, fieldsGender, Address[0], Address[1], Address[2], Address[3]);
                if (Address[4] != "") ErrorText += "Не распознано: " + Address[4];

            } else
            {
                Temp.WriteNew(0, fieldsSnils, filedsFIO[0], filedsFIO[1], filedsFIO[2], fieldsDate, fieldsGender);
            }

            return Temp;

        }

        private static string[] ParseAddres(string Line)
        {
            string[] ret = { "", "", "", "","" };
            string[] LineAddres = Line.Split(' ');

            int lastindex = 4;
            foreach (string item in LineAddres)
            {
                switch (item[0])
                {
                    case '!':ret[0] = item.Substring(1);lastindex = 0; break;
                    case '@': ret[1] = item.Substring(1); lastindex = 1; break;
                    case '#': ret[2] = item.Substring(1); lastindex = 2; break;
                    case '$': ret[3] = item.Substring(1); lastindex = 3; break;
                    default:ret[lastindex] += " "+item;break;
                }
            }

            return ret;
        }

        
        private static string ParseGender(string Line)
        {
            switch (Line) { 
                case "М":return "М";
                case "Ж":return "Ж";
                case "м": return "М";
                case "ж": return "Ж";
               default: return "-";
            }
        }


        private static string ParseData(string Line)
        {
            //:TODO: Придумать чо нить
            return Line;
        }


        private static string ParseSnils(string Line)
        {
            if (Line.IndexOf('-') != -1) Line = Line.Replace("-","");
            if (Line.IndexOf(' ') != -1) Line = Line.Replace(" ", "");

            return Line;
        }



        /// <summary>
        /// Расшифровка Фамилии имени и отчества
        /// </summary>
        /// <param name="Line"></param>
        /// <returns></returns>
        private static string[] ParseFIO(string Line)
        {
            string[] ret = { "", "", "" };
            
            if (Line.IndexOf('!') != -1)
            {
                ret = Line.Split('!');
                for (int i = 0; i < ret.Length; i++) ret[i] = ret[i].Trim();

            } else
            {
                ret = Line.Split(' ');
            }

            return ret;
        }
    }
}
