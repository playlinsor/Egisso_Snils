﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Egisso_snils
{
    class ValidationRequest
    {
        static public int stat_id = 1;

        Int32 id;
        string snils;
        bool isSnils;
        string family;
        string name;
        string lastname;
        string date;
        string gender;
        string addressCity;
        string addressRaion;
        string addressOblast;
        string addressStrana;

        public void WriteNew(int Number,string Snils,string Family,string Name,string LastName,string Date,string Gender)
        {
            id = Number;
            snils = Snils;
            isSnils = true;
            family = Family;
            name = Name;
            lastname = LastName;
            date = Date;
            gender = Gender;
        }

        public void WriteNew(int Number, string Family, string Name, string LastName, string Date, string Gender,string AddressCity,string AddressRaion,string AddressOblast,string AddressStrana)
        {
            id = Number;
            isSnils = false;
            family = Family;
            name = Name;
            lastname = LastName;
            date = Date;
            gender = Gender;
            addressCity = AddressCity;
            addressRaion = AddressRaion;
            addressOblast = AddressOblast;
            addressStrana = AddressStrana;
        }

        public string GetFIOandId()
        {
            return string.Format("{0}: {1} {2} {3}", id, family, name, lastname);
        }

        public string GetTextValidation()
        {
            if (isSnils)
            {
                return string.Format(Templates.SnilsTemplate, stat_id++, snils, family.ToUpper(), name.ToUpper(), lastname.ToUpper(), date.ToUpper(), gender.ToUpper());
            }
            else
            {
                return string.Format(Templates.AddressTemplate, stat_id++, family.ToUpper(), name.ToUpper(), lastname.ToUpper(), date, gender,addressCity.ToUpper(), addressRaion.ToUpper(), addressOblast.ToUpper(), addressStrana.ToUpper());
            }
        }
    }

}
